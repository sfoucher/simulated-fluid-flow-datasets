import pydantic
from typing import Optional

class Subject(pydantic.BaseModel):
    id: Optional[int]
    title: str
    description: str