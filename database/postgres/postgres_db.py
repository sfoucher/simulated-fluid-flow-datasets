import logging
import uuid
from typing import Optional

import pydantic
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker, Session

import database.postgres.orm.miscellaneous as msc
import database.postgres.orm.scraper.actions as osa
import scraper.actions as sa
from database.abstract_database import Database
from database.postgres.orm import Base

logger = logging.getLogger(__name__)


class PostgresDBConfig(pydantic.BaseModel):
    username: str = 'aeg'
    password: str = 'ioqhLE4Es7ik5UXH_'
    host: str = '127.0.0.1'  # 'localhost'
    port: pydantic.conint(strict=True, ge=1, le=65535) = 5432
    database: str = 'aeg_db'
    dialect: str = 'postgresql'
    driver: str = 'psycopg2'

    create_tables: bool = True


class PostgresDB(Database):
    def __init__(self, config: PostgresDBConfig):
        self.config = config
        self.database_url = f"{config.dialect}+{config.driver}://" \
                            f"{config.username}:{config.password}@{config.host}:{config.port}/{config.database}"

        self.engine = create_engine(
            self.database_url
        )
        self.session_factory = sessionmaker(self.engine)

        if self.config.create_tables:
            Base.metadata.create_all(self.engine)

    def create_new_subject(self, title: str, description: Optional[str] = None) -> int:
        orm_subject = msc.SubjectOrm(title=title, description=description)
        try:
            with Session(self.engine) as session:
                session.add(orm_subject)
                session.commit()
                subjct_id = orm_subject.id
        except IntegrityError as ie:
            logger.error(ie)
            raise ValueError(f"Title {title} already exists")
        return subjct_id

    def submit_scraper_task(self, task: sa.ScraperAction) -> uuid.UUID:
        if isinstance(task, sa.SearchTask):
            orm_task = osa.SearchTaskOrm(status=osa.SearchTaskStatus.QUEUED, **task.dict())
            with Session(self.engine) as session:
                session.add(orm_task)
                session.commit()
            return task.id
        else:
            raise NotImplementedError(f"Cannot handle a scraper task of type {type(task)}")

    def submit_search_result(self, search_result: sa.SearchResult) -> uuid.UUID:
        orm_search_result = osa.SearchResultOrm(**search_result.dict())
        with Session(self.engine) as session:
            session.add(orm_search_result)
            session.commit()
            sr_id = search_result.id
        return sr_id

    def retrieve_scraper_search_task_by_id(self, id: uuid.UUID) -> sa.SearchTask:
        with Session(self.engine) as session:
            search_task = session.query(osa.SearchTaskOrm).filter(osa.SearchTaskOrm.id == id).first()

        return sa.SearchTask.from_orm(search_task)

    def retrieve_scraper_search_task(self) -> sa.SearchTask:
        with Session(self.engine) as session:
            search_task = session.query(osa.SearchTaskOrm).order_by(osa.SearchTaskOrm.submission_timestamp).first()

        return sa.SearchTask.from_orm(search_task)
