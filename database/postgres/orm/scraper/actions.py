import enum
import uuid
from datetime import datetime, timezone

from sqlalchemy import Column, Text, Integer, Enum, ForeignKey
from sqlalchemy.dialects.postgresql import ARRAY, UUID, TIMESTAMP

from database.postgres.orm import Base


class SearchTaskStatus(enum.Enum):
    QUEUED = "Queued"
    PROCESSING = "Processing"
    COMPLETED = "Completed"
    ERROR = "Error"


class SearchTaskOrm(Base):
    __tablename__ = "search"
    id = Column(UUID(as_uuid=True),
                primary_key=True,
                default=uuid.uuid4)
    search_term = Column(Text, nullable=False)
    subject_id = Column(Integer, ForeignKey("subjects.id"), index=True, nullable=False)
    submission_timestamp = Column(TIMESTAMP,
                                  index=True,
                                  nullable=False,
                                  default=datetime.now(timezone.utc))
    status = Column(Enum(SearchTaskStatus))
    completion_timestamp = Column(TIMESTAMP)


class SearchResultOrm(Base):
    __tablename__ = "search_results"
    id = Column(UUID(as_uuid=True),
                primary_key=True,
                default=uuid.uuid4)
    subject_id = Column(Integer, ForeignKey("subjects.id"), index=True, nullable=False)
    url = Column(Text, nullable=False)
    texts = Column(ARRAY(Text, dimensions=1))
    classification = Column(ARRAY(Integer, dimensions=1))
    comment = Column(Text)
    search_task_id = Column(UUID(as_uuid=True), ForeignKey("search.id"), nullable=False)
    timestamp= Column(TIMESTAMP,
                      nullable=False,
                      default=datetime.now(timezone.utc))
