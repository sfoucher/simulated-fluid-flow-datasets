from datetime import datetime, timezone

from sqlalchemy import Column, Text, Integer
from sqlalchemy.dialects.postgresql import TIMESTAMP

from database.postgres.orm import Base


class SubjectOrm(Base):
    __tablename__ = "subjects"
    id = Column(Integer,
                primary_key=True,
                autoincrement=True)
    title = Column(Text, unique=True, nullable=False)
    description = Column(Text, nullable=True)
    creation_timestamp = Column(TIMESTAMP,
                                nullable=False,
                                default=datetime.now(timezone.utc))
