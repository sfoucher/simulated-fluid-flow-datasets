from neo4j import GraphDatabase

from database.postgres.postgres_db import PostgresDB, PostgresDBConfig


class BasicDatabase(PostgresDB):
    def __init__(self, config: PostgresDBConfig):
        super(BasicDatabase, self).__init__(config)
        self.neo4j_driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "af35ehje5"))

    def save_relation(self, a, rel, b):
        with self.neo4j_driver.session() as session:
            greeting = session.write_transaction(self._create_and_return_greeting, a, rel, b)
            print(greeting)

    @staticmethod
    def _create_and_return_greeting(tx, a, rel, b):
        result = tx.run("MERGE (a:Entity{name:$a})", a=a)
        result = tx.run("MERGE (b:Entity{name:$b}) ", b=b)
        result = tx.run(f"MATCH (a:Entity),(b:Entity) where a.name=$a and b.name=$b CREATE (a)-[r:{rel.replace(' ','_')}]->(b)", a=a,
                        b=b)
