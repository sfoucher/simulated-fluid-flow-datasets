import uuid
from abc import abstractmethod
from typing import Optional

import scraper.actions as sa


class Database:

    @abstractmethod
    def create_new_subject(self, title: str, description: Optional[str] = None) -> int:
        ...

    @abstractmethod
    def submit_scraper_task(self, task: sa.ScraperAction) -> uuid.UUID:
        ...

    @abstractmethod
    def submit_search_result(self, search_result: sa.SearchResult) -> uuid.UUID:
        ...

    @abstractmethod
    def retrieve_scraper_search_task_by_id(self, id: uuid.UUID) -> sa.SearchTask:
        ...

    @abstractmethod
    def retrieve_scraper_search_task(self) -> sa.SearchTask:
        ...
