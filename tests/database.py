import uuid
import unittest

import scraper.actions as sa
from database.postgres.postgres_db import PostgresDB, PostgresDBConfig


class PostgresMethods(unittest.TestCase):
    def test_new_subject_creation(self):
        db = PostgresDB(PostgresDBConfig())
        subject_id = db.create_new_subject(f"Test{uuid.uuid4()}", "This is a test")
        self.assertIsNotNone(subject_id)

    def test_search_task_submission(self):
        search_task = sa.SearchTask(search_term="Blabla", subject_id=1)
        db = PostgresDB(PostgresDBConfig())
        db.submit_scraper_task(search_task)
        ret_search_task = db.retrieve_scraper_search_task_by_id(search_task.id)
        self.assertTrue(ret_search_task.id == search_task.id)
        self.assertTrue(ret_search_task.subject_id == search_task.subject_id)
        self.assertTrue(ret_search_task.search_term == search_task.search_term)

