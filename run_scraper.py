from scraper.web_scraper import WebScraper
from database.basic.basic_db import PostgresDB, PostgresDBConfig
from database.basic.basic_db import BasicDatabase
import scraper.actions as sa

if __name__ == "__main__":
    database =BasicDatabase(PostgresDBConfig())
    # database.save_relation("Albert","pushed","Robert")
    search_task = sa.SearchTask(search_term="Henry Ford", subject_id=1)
    database.submit_scraper_task(search_task)
    search_task = database.retrieve_scraper_search_task_by_id(search_task.id)

    web_scraper = WebScraper(database=database)
    web_scraper.execute(search_task)
