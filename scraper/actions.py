import uuid
from datetime import datetime, timezone
from typing import List, Optional

import pydantic


class ScraperAction(pydantic.BaseModel):
    id: uuid.UUID = pydantic.Field(default_factory=uuid.uuid4)
    # Subject ID to which this action corresponds to
    subject_id: int  # Use subject_id = 1 for testing


class SearchTask(ScraperAction):
    search_term: str
    submission_timestamp: Optional[datetime]
    completion_timestamp: Optional[datetime]

    class Config:
        orm_mode = True


class SearchResult(ScraperAction):
    # URL from which the information has been retrieved
    url: str
    # List of texts retrieved
    texts: List[str]
    # List containing a class for each entry in the text list above
    classification: Optional[List[int]]
    # Optional comment generated automatically during the scraping process
    comment: Optional[str]
    # Search task leading to this result
    search_task_id: uuid.UUID
    # Timestamp at which the result was processed
    timestamp: datetime = pydantic.Field(default_factory=lambda: datetime.now(timezone.utc))

    class Config:
        orm_mode = True
