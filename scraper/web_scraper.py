import logging
import time
import re

import scraper.actions as sa
from scraper.browser import Browser
from database.abstract_database import Database
from experimental.relation_extraction.open_nre import OpenNRE

logger = logging.getLogger(__name__)


class WebScraper():

    def __init__(self, database: Database):
        self.browser = Browser()
        self.browser.goto("https://www.google.com/")
        self.browser.load_cookies()
        self.database = database
        self.rel_ext = OpenNRE()

    def execute(self, task: sa.ScraperAction):
        if isinstance(task, sa.SearchTask):
            search_results = self.browser.google_search(task.search_term)
            logger.debug(f"{len(search_results)} search results for {task.search_term} (task ID: {task.id})")
            # self.browser.extract_text("https://arxiv.org/pdf/1511.08228.pdf")
            for sr in search_results:
                texts = self.browser.extract_text(sr.url)
                result = sa.SearchResult(url=sr.url, texts=texts, subject_id=task.subject_id, search_task_id=task.id)
                self.database.submit_search_result(result)
                # text = re.sub('\s+',' ', ' '.join(texts))[:5000]
                # res = self.rel_ext.get_relations(text)
                # for rel in res['relations']:
                #     self.database.save_relation(rel['source'], rel['type'], rel['target'])
                # self.database.save_relation("Albert","pushed","Robert")
                # time.sleep(6)
        else:
            raise NotImplementedError(f"Cannot handle tasks of type {type(task)}")