import logging
import os
import pickle
import time
from typing import Any, List, NamedTuple, Optional, Tuple, Union

from browsermobproxy import Server
from bs4 import BeautifulSoup
from bs4.element import Comment, Tag, NavigableString
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import default_values as dv

logger = logging.getLogger(__name__)


class SearchResult(NamedTuple):
    title: str
    url: str


class Browser:
    def __init__(self,
                 headless: bool = False,
                 use_proxy: bool = False,
                 implicit_wait: float = 10,
                 window_size: Tuple[int, int] = dv.WINDOW_SIZE):

        options = Options()
        options.headless = headless
        options.add_argument(f"--window-size={','.join(map(str, window_size))}")

        profile = webdriver.FirefoxProfile()
        profile.set_preference("media.volume_scale", "0.0")

        if use_proxy:
            server = Server(dv.BROWSER_PROXY_PATH)
            server.start()
            self.proxy = server.create_proxy()
            self.proxy.new_har("test", options={'captureHeaders': True, 'captureContent': True})
            self.driver = webdriver.Firefox(options=options, executable_path=dv.GECKODRIVER_PATH,
                                            firefox_profile=profile, proxy=self.proxy.selenium_proxy())

        else:
            self.driver = webdriver.Firefox(options=options, executable_path=dv.GECKODRIVER_PATH,
                                            firefox_profile=profile)

        # Poll the DOM for a certain amount of time when trying to find any element (or elements)
        # not immediately available.
        self.driver.implicitly_wait(implicit_wait)

    def __del__(self):
        self.driver.quit()

    def dump_cookies(self, cookie_file: str = dv.COOKIE_FILE):
        """
            Dump cookies into a file
        :param cookie_file: File to dump the cookies into
        """
        with open(cookie_file, 'wb') as f:
            pickle.dump(self.driver.get_cookies(), f)

        logger.debug(f"Dumped cookies into file {cookie_file}")

    def load_cookies(self, cookie_file: str = dv.COOKIE_FILE):
        """
            Load cookies from a file
        :param cookie_file: File to load cookies from
        """
        if os.path.isfile(cookie_file):
            with open(cookie_file, "rb") as f:
                cookies = pickle.load(f)

            for cookie in cookies:
                self.driver.add_cookie(cookie)
            else:
                logger.warning(f"Could not find file {cookie_file} to load cookies form.")

    def goto(self, url: str, page_loaded_condition: Any = None):
        self.driver.get(url)

        # Wait for the page to be loaded
        if page_loaded_condition:
            try:
                # Wait for page to load
                WebDriverWait(self.driver, 3).until(page_loaded_condition)
            except TimeoutException as te:
                logger.debug(f"Timeout for url: {url}")
                raise te

    def google_search(self, search_text: str) -> List[SearchResult]:
        start_time = time.perf_counter()

        self.goto("https://www.google.com/")
        # Skip annoying user agreement
        # self.driver.find_elements_by_xpath(f'//button')[-1].click()
        search_field = self.driver.find_element_by_xpath(f'//input[@autofocus]')
        search_field.send_keys(search_text)
        search_field.send_keys(Keys.ENTER)

        try:
            # Wait for page to load
            myElem = WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, 'result-stats')))
            pass
        except TimeoutException:
            pass

        search_result_links = self.driver.find_elements_by_xpath("//div[@class='g']/div/div/div/a")

        # List of title and link pairs
        search_results = [
            SearchResult(title=x.find_element_by_tag_name('h3').get_attribute("innerHTML"),
                         url=x.get_attribute('href'))
            for x in
            search_result_links]

        end_time = time.perf_counter()
        logger.debug(f"Google search lead to {len(search_results)} results in {end_time - start_time:0.3f} seconds")

        return search_results

    def extract_text(self, url: Optional[str] = None) -> str:
        start_time = time.perf_counter()

        if url is not None:
            self.goto(url)
            time.sleep(1)
        soup = BeautifulSoup(self.driver.page_source, 'html.parser')

        # text = []
        #
        # def tag_visible(element):
        #     if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        #         return False
        #     if isinstance(element, Comment):
        #         return False
        #     return True

        # visible_texts = filter(tag_visible, soup.findAll(text=True))
        texts = []
        # Element types that can be ignored when searching for relevant text
        FILTERED_ELEMENT_TYPES = ['style', 'script', 'head', 'title', 'meta', '[document]', 'button']

        for elem in soup.findAll(text=True):
            if isinstance(elem, Comment) or elem.parent.name in FILTERED_ELEMENT_TYPES:
                continue
            elif str(elem) == '\n' or str(elem).isspace():
                continue
            else:
                texts.append(elem)

        js = """Array.from(document.getElementsByTagName("a")).map(x => x.removeAttribute("href"))"""
        self.driver.execute_script(js)

        for elem in texts:
            try:
                if elem.parent.has_attr('id'):
                    self.driver.execute_script(
                        f"document.getElementById('{elem.parent.get('id')}').style.backgroundColor = 'red'")
                else:
                    # js = """var aTags = document.getElementsByTagName("%s");
                    #         var searchText = "SearchingText";
                    #         var found;
                    #         for (var i = 0; i < aTags.length; i++) {
                    #           if (aTags[i].textContent == '%s') {
                    #             found = aTags[i];
                    #             break;
                    #           }
                    #         }
                    #         found.style.backgroundColor = 'red'
                    #         """ % (elem.parent.name, str(elem).replace('\n','\\n'))
                    js = """
                        const elem = document.evaluate('%s', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        elem.style.backgroundColor = '#3ed641'
                        elem.onclick = function(){ elem.style.backgroundColor = 'red';}
                    """ % self.xpath_to_elem(elem)
                    self.driver.execute_script(js)
            except Exception as ex:
                print(ex)

        # text = u" ".join(t.strip() for t in visible_texts)
        # text = re.sub('\n+', '\n', u"".join(visible_texts))
        # cleaned_text = [x.replace('\n', '') for x in texts]

        # def recursiveChildren(x):
        #     if "childGenerator" in dir(x):
        #         for child in x.childGenerator():
        #             name = getattr(child, "name", None)
        #             if name in ['meta', 'style', 'title', '[document]', 'button', 'nav', 'script', 'header']:
        #                 continue
        #             recursiveChildren(child)
        #     else:
        #         if not x.isspace() and len(x) > 50:  # Just to avoid printing "\n" parsed from document.
        #             text.append(x)
        #
        # recursiveChildren(soup)
        # for child in soup.recursiveChildGenerator():
        #     name = getattr(child, "name", None)
        #     if name is not None:
        #         continue
        #     elif not child.isspace() and len(child)>20: # leaf node, don't print spaces
        #         print(child)
        end_time = time.perf_counter()
        logger.debug(f"Extracted text from {self.driver.current_url} in {end_time - start_time:0.3f} seconds")

        return texts

    def xpath_to_elem(self, element: Union[Tag, NavigableString]) -> str:
        """
            Generate xpath from BeautifulSoup4 element.
            :param element: BeautifulSoup4 element.
            :return: xpath as string
        """

        components = []
        child = element if element.name else element.parent
        for parent in child.parents:  # type: bs4.element.Tag
            siblings = parent.find_all(child.name, recursive=False)
            components.append(
                child.name if 1 == len(siblings) else '%s[%d]' % (
                    child.name,
                    next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
            child = parent
        components.reverse()
        return '/%s' % '/'.join(components)
