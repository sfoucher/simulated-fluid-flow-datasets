## Known Issues
#### Program crashes with the following error

```Process finished with exit code 139 (interrupted by signal 11: SIGSEGV)```

This is caused by a problem with the compatibility between [NeuralCoref](https://github.com/huggingface/neuralcoref#install-neuralcoref-from-source) and SpaCy.
I my case building NeuralCoref from source fixed the issue (see [issue](https://github.com/huggingface/neuralcoref/issues/167))